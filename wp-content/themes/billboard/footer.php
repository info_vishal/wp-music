<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage BillBoard
 * @since 1.0.0
 */

?>
<!-- Footer Start -->
		<footer id="site-footer" class="site-footer">
			<div class="container footer-content">
				<div class="cover-image">
					<a href="#" id="image_footer">
						<img src="<?php echo get_template_directory_uri(); ?>/images/lana-del-rey-u-bb20-cov-2019-ucbam-billboard-400.jpg" alt="Subscribe to Billboard">
					</a>
				</div>
				<div class="subscribe">
					<h2>Magazine</h2>
					<ul>
						<li><a class="site-footer_link" href="#" title="Customer Service">Customer Service</a></li>
						<li><a class="site-footer_link" href="#" title="Subscribe" id="subscribe_footer">Subscribe</a></li>
						<li><a class="site-footer_link" href="#" title="Buy this issue">Buy this issue</a></li>
					</ul>
				</div>
				<nav class="footer-nav">
					<h2>Sharing Billboard</h2>
					<ul>
						<li><a class="site-footer_link" href="http://www.facebook.com/Billboard" target="blank">Facebook</a></li>
						<li><a class="site-footer_link" href="https://twitter.com/billboard" target="blank">Twitter</a></li>
						<li><a class="site-footer_link" href="http://youtube.com/billboard" target="blank">YouTube</a></li>
						<li><a class="site-footer_link" href="http://instagram.com/billboard" target="blank">Instagram</a></li>
						<li><a class="site-footer_link" href="http://billboard.tumblr.com/" target="blank">Tumblr</a></li>
						<li><a class="site-footer_link" href="http://pinterest.com/billboard" target="blank">Pinterest</a></li>
						<li><a class="site-footer_link" href="https://open.spotify.com/user/billboard.com/playlist/6UeSakyzhiEt4NB3UAd6NQ?si=DmcjQeieS42SKYmpWjvfwg" target="blank">Spotify</a></li>
						<li><a class="site-footer_link" href="https://www.billboard.com/newsletter/preferences">Newsletter</a></li>
					</ul>
				</nav>
				<nav class="footer-nav">
					<h2>Explore Billboard</h2>
					<ul>
						<li><a class="site-footer_link" href="#" target="blank">Shop</a></li>
						<li><a class="site-footer_link" href="#">News Archive</a></li>
						<li><a class="site-footer_link" href="#">Photos Archive</a></li>
						<li><a class="site-footer_link" href="#">Videos Archive</a></li>
						<li><a class="site-footer_link" href="#" target="blank">Magazine Archive</a></li>
						<li><a class="site-footer_link" href="#">Charts Archive</a></li>
					</ul>
				</nav>
				<nav class="footer-nav">
					<h2>Information</h2>
					<ul>
						<li><a class="site-footer_link" href="#" target="blank">Jobs</a></li>
						<li><a class="site-footer_link" href="#" target="blank">Chart Licensing</a></li>
						<li><a class="site-footer_link" href="#" target="blank">Billboard Events</a></li>
						<li><a class="site-footer_link" href="#">Contact Us</a></li>
						<li><a class="site-footer_link" href="#">Sitemap</a></li>
						<li><a class="site-footer_link" href="#">FAQ</a></li>
						<li><a class="site-footer_link" href="#" target="blank">Feedback</a></li>
						<li><a class="site-footer_link" href="#" target="blank">Lyrics</a></li>
					</ul>
				</nav>
				<div class="footer-brand">
					<div class="billboard brand">
						<a class="site-footer_link" href="#">Billboard</a>
					</div>
					<a href="#" title="The Hollywood Reporter" target="_blank" class="site-footer_link thr"></a>
					<div class="affiliates">
						<a href="http://www.spin.com/" title="Spin.com" target="_blank" class="site-footer_link spin"></a>
						<a href="http://www.vibe.com/" title="Vibe.com" target="_blank" class="site-footer_link vibe"></a>
						<a href="http://www.stereogum.com/" title="Stereogum.com" target="_blank" class="site-footer_link stereogum"></a>
					</div>
				</div>
			</div>
			<div class="container">
				<p class="copyright_paragraph">© 2019 Billboard. All Rights Reserved.</p>
				<p class="copyright_paragraph">
					<a class="copyright_link" href="#">Terms of Use</a>
					<a class="copyright_link" href="#">Privacy Policy</a>
					<a class="copyright_link" href="#">About Our Ads</a>
					<a class="copyright_link copyright_link-contrast" href="#">Advertising</a>
				</p>
			</div>
			<div class="container">
				<p class="station-identification">
					Billboard is a subsidiary of Prometheus Global Media, LLC.
				</p>
			</div>
		</footer>
		<!-- Footer Ends -->
	</div>
	
	<script>
		jQuery(document).ready(function(){
			var video_closed = false;
			// Search Form Toggle jQuery
			jQuery('.site-header_search-toggle').click(function(){
				jQuery('.site-header').addClass('is-search-visible');
			});
			jQuery('.js-search-submit').click(function(){
				jQuery('.site-header').removeClass('is-search-visible');
			});
			
			// Chart Popup jQuery
			jQuery('.how-it-works-modal-toggle').click(function(){
				jQuery('.how-it-works-modal').toggleClass('how-it-works-modal-visible');
			});
			
			// Date Picker Popup jQuery
			jQuery('.date-search-modal-toggle').click(function(){
				jQuery('.date-search-modal').toggle();
			});
			
			// Mobile Navigation jQuery
			jQuery('.site-header_nav-toggle').click(function(){
				jQuery('.site-header').toggleClass('is-nav-visible');
				jQuery('body').toggleClass('body-fixed');
			});
			
			// Listing Accordion jQuery
			jQuery('.site-header_nav-item').click(function() { 
				if ($(this).hasClass('collapsed')) {   
					$(this).removeClass('collapsed');
					$(this).addClass('expanded');
				} else {
					$(this).addClass('collapsed');
					$(this).removeClass('expanded');
				}
			});
							
			// Floating Video jQuery				
			var $window = $(window);
			var $videoWrap = $('.video-wrap');
			var $video = $('.video');
			var videoHeight = $video.outerHeight();
			$window.on('scroll',  function() {
				if(video_closed == true){
					return;
				}
				var windowScrollTop = $window.scrollTop();
				var videoBottom = videoHeight + $videoWrap.offset().top;
				if (windowScrollTop > videoBottom) {
					//jQuery('.video-wrap').show();
					$videoWrap.height(videoHeight);
					$video.addClass('stuck');
				} else {
					$videoWrap.height('auto');
					$video.removeClass('stuck');
				}
			});
			jQuery('.floating-chart-video_close').click(function(){
				jQuery('.video').removeClass('stuck');
				video_closed = true;
			});
			/*Floating Code for Iframe End*/
			
			// Listing Accordion jQuery
			jQuery('.chart-list-item_first-row').click(function() { 
				$(this).next('.chart-list-item_extra-info').toggleClass('active');
				$(this).parent('.chart-list-item').toggleClass('chart-list-item-expanded');
			});
			
			// Mobile Search Focuse jQuery
			jQuery('.site-header_nav_extra-mobile-menu_search').focusin(function(){
				jQuery(this).addClass('site-header_nav_extra-mobile-menu_search-active');
			});
			jQuery('.site-header_nav_extra-mobile-menu_search').focusout(function(){
				jQuery(this).removeClass('site-header_nav_extra-mobile-menu_search-active');
			});

			jQuery('.play_on_click').click(function() {
				jQuery('#playing_iframe').attr('src', "https://www.youtube.com/embed/"+this.id);
				jQuery('.floating-chart-video_title').text(this.title);
				jQuery('.floating-chart-video_rank').text($(this).data('val'));
				video_closed = false;
				window.scrollBy(0,10);
			});
			
			// Hide Header On Scroll Down jQuery
			var didScroll;
			var lastScrollTop = 0;
			var delta = 5;
			var navbarHeight = $('header').outerHeight();
			$(window).scroll(function(event){
				didScroll = true;
			});
			setInterval(function() {
				if (didScroll) {
					hasScrolled();
					didScroll = false;
				}
			}, 250);
			function hasScrolled() {
				var st = $(this).scrollTop();
				// Make sure they scroll more than delta
				if(Math.abs(lastScrollTop - st) <= delta)
					return;
				// If they scrolled down and are past the navbar, add class .nav-up.
				// This is necessary so you never see what is "behind" the navbar.
				if (st > lastScrollTop && st > navbarHeight){
					// Scroll Down
					$('body').addClass('hidden-nav');
					$('.header-wrapper').addClass('hidden');
					$('header').addClass('is-scrolling').addClass('is-scrolling-down');
					$('.header-wrapper_secondary-header').addClass('hidden');
				} else {
					// Scroll Up
					if(st + $(window).height() < $(document).height()) {
						$('body').removeClass('hidden-nav');
						$('.header-wrapper').removeClass('hidden');
						$('header').removeClass('is-scrolling').removeClass('is-scrolling-down');
					$('.header-wrapper_secondary-header').removeClass('hidden');
					}
				}
				lastScrollTop = st;
			}
		});
	</script>
		
</body>
</html>