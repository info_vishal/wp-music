<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage BillBoard
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_template_directory_uri(); ?>/css/video-js.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" type="text/css">
	
	<!-- JS Files -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/jQuery.js"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
	<div class="chart-page chart-page-is-content-initialized hidden-nav-secondary">
		<div class="header-wrapper">
				<header id="site-header" class="site-header" role="banner">
					<div class="site-header_background">
						<div class="site-header_contents">
							<div class="site-header_bar">
								<button class="site-header_nav-toggle"><i class="fa fa-bars"></i></button>
								<div class="site-header_brand">
								<a class="site-header_brand-link" href="#">
									<?php 
										$custom_logo_id = get_theme_mod( 'custom_logo' );
										$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
									?>
									<img class="site-header_brand-logo" src="<?php echo $logo[0];?>" alt="Billboard">
									<span class="site-header_brand-name">Billboard</span>
								</a>
								</div>
								<div class="site-header_useractions">
									<div class="site-header_useractions_desktop">
										<div class="site-header_useractions_desktop_logged-out bb-user-anon">
											<span class="site-header_useraction">
												<a href="#" class="site-header_useraction-user-subscribe-upgrade bb-user-elevate-link subscribe_header">Subscribe</a>
											</span>
										</div>
									</div>
								</div>
								<button class="site-header_search-toggle"><i class="fa fa-search"></i></button>
							</div>
							<nav class="site-header_nav">
								<div class="site-header_nav-inner">
									<?php 
										wp_nav_menu(array('menu' => "2",
										    'menu_class' => "site-header_nav-items",
										    'container' => "ul",
										) );
									?>
									<ul class="site-header_nav-items site-header_nav-items-more" style="">
										<li class="site-header_nav-item collapsed  site-header_nav-item_parent">
											<span class="site-header_nav-link-more site-header_nav-link">More</span>
											<span class="carat"></span>
											<ul class="site-header_subnav-items site-header_subnav-items_child">
												<li class="site-header_subnav-item    hidden">
													<a href="#" class="site-header_subnav-link">Charts</a>
												</li>
												<li class="site-header_subnav-item    hidden">
													<a href="#" class="site-header_subnav-link">News</a>
												</li>
												<li class="site-header_subnav-item    hidden">
													<a href="#" class="site-header_subnav-link">Video</a>
												</li>
												<li class="site-header_subnav-item    hidden">
													<a href="#" class="site-header_subnav-link">Photos</a>
												</li>
												<li class="site-header_subnav-item    hidden">
													<a href="#" class="site-header_subnav-link">Business</a>
												</li>
												<li class="site-header_subnav-item    hidden">
													<a href="#" class="site-header_subnav-link">Newsletters</a>
												</li>
											</ul>
										</li>
									</ul>
								</div>
								<div class="site-header_nav_extra-mobile-menu">
									<div class="site-header_nav_extra-mobile-menu_search">
										<form class="js-search-form" method="get" accept-charset="UTF-8">
											<input class="js-search-input" placeholder="Search..." maxlength="255">
											<button class="js-search-submit"><i class="fa fa-search hamburger-menu-icon"></i></button>
										</form>
									</div>
									<div class="site-header_nav_extra-mobile-menu_hr"></div>
									<div class="site-header_nav_extra-mobile-menu_useractions bb-user-anon ">
										<div class="site-header_nav_extra-mobile-menu_useraction site-header_nav_extra-mobile-menu_useraction-gray bb-user-elevate-link-holder">
											<a class="bb-user-elevate-link" href="#">Subscribe</a>
										</div>
									</div>
									<div class="site-header_nav_extra-mobile-menu_useractions bb-user-free bb-user-paid bb-user-hidden">
										<div class="site-header_nav_extra-mobile-menu_useraction site-header_nav_extra-mobile-menu_useraction-gray">
											<a class="bb-user-profile-link" href="#">View Profile</a>
										</div>
									</div>
									<div class="site-header_nav_extra-mobile-menu_hr"></div>
									<div class="site-header_nav_extra-mobile-menu_social">
										<h2>Follow Billboard</h2>
										<ul>
											<li class="site-header_nav_extra-mobile-menu_social_icon"><a href="https://www.facebook.com/Billboard/"><i class="fa fa-facebook"></i></a></li>
											<li class="site-header_nav_extra-mobile-menu_social_icon"><a href="https://www.twitter.com/billboard/"><i class="fa fa-twitter"></i></a></li>
											<li class="site-header_nav_extra-mobile-menu_social_icon"><a href="https://www.instagram.com/billboard/"><i class="fa fa-instagram"></i></a></li>
											<li class="site-header_nav_extra-mobile-menu_social_icon"><a href="https://www.youtube.com/channel/UCsVcseUYbYjldc-XgcsiEbg"><i class="fa fa-youtube"></i></a></li>
										</ul>
									</div>
									<div class="site-header_nav_extra-mobile-menu_hr"></div>
									<div class="site-header_nav_extra-mobile-menu_about">
										<h2>About Billboard</h2>
										<ul>
											<li><a class="copyright_link" href="/terms-of-use">Terms of Use</a></li>
											<li><a class="copyright_link" href="/privacy-policy">Privacy Policy</a></li>
											<li><a class="copyright_link" href="/about-our-ads">About Our Ads</a></li>
											<li><a class="copyright_link copyright_link-contrast" href="#">Advertising</a></li>
										</ul>
										<p>© 2019 Billboard. All Rights Reserved.<br> Billboard is a subsidiary of Valence Media, LLC.</p>
									</div>
								</div>
							</nav>
							<form class="site-header_search-form site-header_search-form-main js-search-form">
								<input class="site-header_search-input js-search-input" placeholder="Search..." maxlength="255">
								<button id="site-header_search-submit" class="site-header_search-submit js-search-submit"><i class="fa fa-times"></i></button>
							</form>
						</div>
					</div>
				</header>
				<div class="header-wrapper_secondary-header">
					<nav class="site-header-links">
						<div class="site-header-links_contents bb-user-anon bb-user-free ">
							<?php 
								wp_nav_menu(array('menu' => "4",
								    'menu_class' => "",
								    'container' => "ul",
								) );
							?>
						</div>
					</nav>
				</div>
			</div>
			<div class="site-header_placeholder" style="height: 83px;"></div>
			<div class="chart-piano-overlay_attachment-point"></div>
