<?php 
/* Template Name: Home Page */
get_header();
?>
<?php

$the_query = new WP_Query( array(
	    'post_type' => 'song',
	    'orderby'   => 'meta_value_num',
	    'meta_key'  => 'last_week_1',
	) );
$posts = $the_query->posts;

wp_reset_postdata();
 
?>
<style type="text/css" media="screen">
.chart-details{
	display: inline-block !important;
}
.chart-details_left-rail {
		display:inline-block !important;
}.bb_sidebar{
	vertical-align: top;
	display: inline-block !important;
}
</style>
<main id="main" class="page-content">
	<div class="chart-detail-header">
		<div class="chart-detail-header_background chart-detail-header_background-hot-100"></div>
		<div class="chart-detail-header_top-bar">
			<div class="container container-no-background container-relative">
				<span class="chart-detail-header_top-bar-back">
					<a href="#"><span class="fa fa-arrow-left"></span> All Charts</a>
				</span>
				<span class="how-it-works-modal-toggle chart-detail-header_top-bar-how-it-works">
					<span class="fa fa-info-circle"></span>
				</span>
				<div id="howItWorksModal" class="how-it-works-modal">
					<div class="how-it-works-modal_top-row">
						<div class="how-it-works-modal_title">How This Chart Works</div>
						<div class="how-it-works-modal_close-icon how-it-works-modal-toggle"><span class="fa fa-close"></span></div>
					</div>
					<div class="how-it-works-modal_headline">Radio Airplay + Sales Data + Streaming Data = HOT 100</div>
					<div class="how-it-works-modal_description">
						<p>This week's most popular songs across all genres, ranked by radio airplay audience impressions, as measured by Nielsen Music, sales data as compiled by Nielsen Music, and streaming activity data provided by online music sources.&nbsp;</p>
					</div>
					<div class="how-it-works-modal_legend-page"><a id="legend-page-url" href="#">View Charts Legend</a></div>
					<div class="how-it-works-modal_close-row">
						<button class="how-it-works-modal_close-btn how-it-works-modal-toggle">CLOSE</button>
					</div>
				</div>
				<span class="chart-detail-header_top-bar-share">
					<span class="fa fa-share"></span>
					<div class="social-shares social-shares-closed">
						<a class="social-share social-share-facebook" rel="popup" data-width="400" data-height="450" data-tracklabel="Facebook-share" onclick="" href="#">
							<i class="fa fa-facebook"></i>
						</a>
						<a class="social-share social-share-twitter" rel="popup" data-width="400" data-height="450" data-tracklabel="Twitter-share" href="">
							<i class="fa fa-twitter"></i>
						</a>
						<span class="social-share social-share_close"><i class="fa fa-share"></i></span>
					</div>
				</span>
			</div>
		</div>
		<div class="container container-no-background chart-detail-header_chart-info ">
			<h1 class="chart-detail-header_chart-name">
				<img src="<?php echo get_template_directory_uri(); ?>/images/the-hot-100.svg" alt="The Hot 100"> 
				<span class="chart-detail-header-hidden">HOT 100</span>
			</h1>
			<div class="chart-detail-header_select-date">
				The week of
				<span class="dropdown chart-detail-header_date-selector" data-open="false">
					<button class="chart-detail-header_date-selector-button">
						August 31, 2019
						<span class="chart-detail-header_date-selector-caret fa fa-caret-down"></span></button>
					<label>
						<input type="checkbox">
						<ul class="dropdown_date-selector-inner">
							<li class="dropdown_date-selector-option ">
								<a href="#">
									<span class="fa fa-chevron-left"></span> Last Week
								</a>
							</li>
							<li class="dropdown_date-selector-option dropdown_date-selector-option-disabled">
								<span class="fa fa-chevron-right"></span> Next Week
							</li>
							<li class="dropdown_date-selector-option dropdown_date-selector-option-disabled">
								Current Week
							</li>
							<li class="dropdown_date-selector-option date-search-modal-toggle">
								<span class="fa fa-calendar"></span> Date Search
							</li>
						</ul>
					</label>
				</span>
			</div>
		</div>
		<div class="container container-no-background chart-number-one">
			<!-- Video Start -->
			<div class="chart-video_wrapper">
				<div class="video-wrap">
					<div class="video">
				  		<iframe id="playing_iframe" src="https://www.youtube.com/embed/<?php the_field("youtube_id",$posts[0]->ID); ?>" frameborder="0" gesture="media" allowfullscreen></iframe>
						<div class="floating-chart-video_info">
							<div class="floating-chart-video_rank floating-chart-video-minimized-hide" style="display: inline-block;">1</div>
							<div class="floating-chart-video_mhm-logo floating-chart-video-minimized-hide" style="display: none;"></div>
							<div class="floating-chart-video_description">
								<div class="floating-chart-video_type">Watch &amp; Listen</div>
								<div class="floating-chart-video_title">Bad Guy - Billie Eilish</div>
							</div>
							<div class="floating-chart-video_close floating-chart-video_close-text">
								CLOSE
							</div>
							<div class="floating-chart-video_minimize" style="height: auto;">
								<i class="fa fa-chevron-up" aria-hidden="true"></i>
							</div>
						</div>
					</div>
			  	</div>
			</div>
			<!-- Video Ends -->
			<!-- Video Side Content -->
			<div class="chart-number-one_info ">
				<div class="chart-number-one_stats">
					<div class="chart-number-one_stats-cell chart-number-one_rank">
						<img src="<?php echo get_template_directory_uri(); ?>/images/number-one.svg">
					</div>
					<div class="chart-number-one_stats-cell chart-number-one_stats-cell-bordered">
						<div class="chart-number-one_stats-icon fa fa-arrow-up fa-rotate-45"></div>
						<div class="chart-number-one_last-week">2</div>
						LAST WEEK
					</div>
					<div class="chart-number-one_stats-cell chart-number-one_stats-cell-bordered">
						<div class="chart-number-one_stats-icon fa fa-clock-o"></div>
						<div class="chart-number-one_weeks-on-chart">9</div>
						WEEKS ON CHART
					</div>
				</div>
				<div class="chart-number-one_details">
					<div class="chart-number-one_title"><?php echo $posts[0]->post_title; ?></div>
					<div class="chart-number-one_artist"><?php the_field("lyricist",$posts[0]->ID); ?></div>
					<div class="chart-number-one_lyrics">
						<a href="#">Song Lyrics</a>
					</div>
					<div class="chart-number-one_awards">
						<div class="chart-number-one_award-icon"><i class="fa fa-microphone"></i> Biggest gain in airplay</div>
						<div class="chart-number-one_award-icon"><i class="fa fa-dot-circle-o"></i> Gains in performance</div>
					</div>
				</div>
			</div>
			<!-- Video Side Content Ends -->
		</div>
	</div>
	<!-- Date Picker Popup -->
	<div id="dateSearchModal" class="date-search-modal" data-visible="false" style="opacity: 1; display: none;">
		<div class="date-search-modal_inner">
			<div class="date-search-modal_top-row">
				<div class="date-search-modal_title">
					<span class="fa fa-calendar"></span>
					<br> Chart Archive Search
					<div class="date-search-modal_title-subtext">
						SELECT A DATE
					</div>
				</div>
				<div class="date-search-modal_close-icon date-search-modal-toggle"><span class="fa fa-close"></span></div>
			</div>
			<div class="date-search-modal_main-content">
				<div class="date-search-modal_instructions">
					Try a birthday or anniversary
				</div>
				<div class="date-search-modal_date-selector-row"><span class="dropdown dropdown_date-selector date-search-modal_month-select" data-date-type="month"><button><span class="date-search-modal_selected-option">Aug</span><span class="fa fa-caret-down dropdown_date-selector-caret"></span></button>
					<label>
						<input type="checkbox">
						<ul class="dropdown_date-select-options">
							<li class="date-search-modal_select-option" data-date-val="01">Jan</li>
							<li class="date-search-modal_select-option" data-date-val="02">Feb</li>
							<li class="date-search-modal_select-option" data-date-val="03">Mar</li>
							<li class="date-search-modal_select-option" data-date-val="04">Apr</li>
							<li class="date-search-modal_select-option" data-date-val="05">May</li>
							<li class="date-search-modal_select-option" data-date-val="06">Jun</li>
							<li class="date-search-modal_select-option" data-date-val="07">Jul</li>
							<li class="date-search-modal_select-option" data-date-val="08">Aug</li>
							<li class="date-search-modal_select-option" data-date-val="09">Sep</li>
							<li class="date-search-modal_select-option" data-date-val="10">Oct</li>
							<li class="date-search-modal_select-option" data-date-val="11">Nov</li>
							<li class="date-search-modal_select-option" data-date-val="12">Dec</li>
						</ul>
					</label>
					</span>
					&nbsp;
					<span class="dropdown dropdown_date-selector date-search-modal_day-select" data-date-type="day"><button><span class="date-search-modal_selected-option">31</span><span class="fa fa-caret-down dropdown_date-selector-caret"></span></button>
					<label>
						<input type="checkbox">
						<ul class="dropdown_date-select-options">
							<li class="date-search-modal_select-option" data-date-val="1">1</li>
							<li class="date-search-modal_select-option" data-date-val="2">2</li>
							<li class="date-search-modal_select-option" data-date-val="3">3</li>
							<li class="date-search-modal_select-option" data-date-val="4">4</li>
							<li class="date-search-modal_select-option" data-date-val="5">5</li>
							<li class="date-search-modal_select-option" data-date-val="6">6</li>
							<li class="date-search-modal_select-option" data-date-val="7">7</li>
							<li class="date-search-modal_select-option" data-date-val="8">8</li>
							<li class="date-search-modal_select-option" data-date-val="9">9</li>
							<li class="date-search-modal_select-option" data-date-val="10">10</li>
							<li class="date-search-modal_select-option" data-date-val="11">11</li>
							<li class="date-search-modal_select-option" data-date-val="12">12</li>
							<li class="date-search-modal_select-option" data-date-val="13">13</li>
							<li class="date-search-modal_select-option" data-date-val="14">14</li>
							<li class="date-search-modal_select-option" data-date-val="15">15</li>
							<li class="date-search-modal_select-option" data-date-val="16">16</li>
							<li class="date-search-modal_select-option" data-date-val="17">17</li>
							<li class="date-search-modal_select-option" data-date-val="18">18</li>
							<li class="date-search-modal_select-option" data-date-val="19">19</li>
							<li class="date-search-modal_select-option" data-date-val="20">20</li>
							<li class="date-search-modal_select-option" data-date-val="21">21</li>
							<li class="date-search-modal_select-option" data-date-val="22">22</li>
							<li class="date-search-modal_select-option" data-date-val="23">23</li>
							<li class="date-search-modal_select-option" data-date-val="24">24</li>
							<li class="date-search-modal_select-option" data-date-val="25">25</li>
							<li class="date-search-modal_select-option" data-date-val="26">26</li>
							<li class="date-search-modal_select-option" data-date-val="27">27</li>
							<li class="date-search-modal_select-option" data-date-val="28">28</li>
							<li class="date-search-modal_select-option" data-date-val="29">29</li>
							<li class="date-search-modal_select-option" data-date-val="30">30</li>
							<li class="date-search-modal_select-option" data-date-val="31">31</li>
						</ul>
					</label>
					</span>
					,&nbsp;
					<span class="dropdown dropdown_date-selector date-search-modal_year-select" data-date-type="year"><button><span class="date-search-modal_selected-option">2019</span><span class="fa fa-caret-down dropdown_date-selector-caret"></span></button>
					<label>
						<input type="checkbox">
						<ul class="dropdown_date-select-options">
							<li class="date-search-modal_select-option" data-date-val="2019">2019</li>
							<li class="date-search-modal_select-option" data-date-val="2018">2018</li>
							<li class="date-search-modal_select-option" data-date-val="2017">2017</li>
							<li class="date-search-modal_select-option" data-date-val="2016">2016</li>
							<li class="date-search-modal_select-option" data-date-val="2015">2015</li>
							<li class="date-search-modal_select-option" data-date-val="2014">2014</li>
							<li class="date-search-modal_select-option" data-date-val="2013">2013</li>
							<li class="date-search-modal_select-option" data-date-val="2012">2012</li>
							<li class="date-search-modal_select-option" data-date-val="2011">2011</li>
							<li class="date-search-modal_select-option" data-date-val="2010">2010</li>
							<li class="date-search-modal_select-option" data-date-val="2009">2009</li>
							<li class="date-search-modal_select-option" data-date-val="2008">2008</li>
							<li class="date-search-modal_select-option" data-date-val="2007">2007</li>
							<li class="date-search-modal_select-option" data-date-val="2006">2006</li>
							<li class="date-search-modal_select-option" data-date-val="2005">2005</li>
							<li class="date-search-modal_select-option" data-date-val="2004">2004</li>
							<li class="date-search-modal_select-option" data-date-val="2003">2003</li>
							<li class="date-search-modal_select-option" data-date-val="2002">2002</li>
							<li class="date-search-modal_select-option" data-date-val="2001">2001</li>
							<li class="date-search-modal_select-option" data-date-val="2000">2000</li>
							<li class="date-search-modal_select-option" data-date-val="1999">1999</li>
							<li class="date-search-modal_select-option" data-date-val="1998">1998</li>
							<li class="date-search-modal_select-option" data-date-val="1997">1997</li>
							<li class="date-search-modal_select-option" data-date-val="1996">1996</li>
							<li class="date-search-modal_select-option" data-date-val="1995">1995</li>
							<li class="date-search-modal_select-option" data-date-val="1994">1994</li>
							<li class="date-search-modal_select-option" data-date-val="1993">1993</li>
							<li class="date-search-modal_select-option" data-date-val="1992">1992</li>
							<li class="date-search-modal_select-option" data-date-val="1991">1991</li>
							<li class="date-search-modal_select-option" data-date-val="1990">1990</li>
							<li class="date-search-modal_select-option" data-date-val="1989">1989</li>
							<li class="date-search-modal_select-option" data-date-val="1988">1988</li>
							<li class="date-search-modal_select-option" data-date-val="1987">1987</li>
							<li class="date-search-modal_select-option" data-date-val="1986">1986</li>
							<li class="date-search-modal_select-option" data-date-val="1985">1985</li>
							<li class="date-search-modal_select-option" data-date-val="1984">1984</li>
							<li class="date-search-modal_select-option" data-date-val="1983">1983</li>
							<li class="date-search-modal_select-option" data-date-val="1982">1982</li>
							<li class="date-search-modal_select-option" data-date-val="1981">1981</li>
							<li class="date-search-modal_select-option" data-date-val="1980">1980</li>
							<li class="date-search-modal_select-option" data-date-val="1979">1979</li>
							<li class="date-search-modal_select-option" data-date-val="1978">1978</li>
							<li class="date-search-modal_select-option" data-date-val="1977">1977</li>
							<li class="date-search-modal_select-option" data-date-val="1976">1976</li>
							<li class="date-search-modal_select-option" data-date-val="1975">1975</li>
							<li class="date-search-modal_select-option" data-date-val="1974">1974</li>
							<li class="date-search-modal_select-option" data-date-val="1973">1973</li>
							<li class="date-search-modal_select-option" data-date-val="1972">1972</li>
							<li class="date-search-modal_select-option" data-date-val="1971">1971</li>
							<li class="date-search-modal_select-option" data-date-val="1970">1970</li>
							<li class="date-search-modal_select-option" data-date-val="1969">1969</li>
							<li class="date-search-modal_select-option" data-date-val="1968">1968</li>
							<li class="date-search-modal_select-option" data-date-val="1967">1967</li>
							<li class="date-search-modal_select-option" data-date-val="1966">1966</li>
							<li class="date-search-modal_select-option" data-date-val="1965">1965</li>
							<li class="date-search-modal_select-option" data-date-val="1964">1964</li>
							<li class="date-search-modal_select-option" data-date-val="1963">1963</li>
							<li class="date-search-modal_select-option" data-date-val="1962">1962</li>
							<li class="date-search-modal_select-option" data-date-val="1961">1961</li>
							<li class="date-search-modal_select-option" data-date-val="1960">1960</li>
							<li class="date-search-modal_select-option" data-date-val="1959">1959</li>
							<li class="date-search-modal_select-option" data-date-val="1958">1958</li>
							<li class="date-search-modal_select-option" data-date-val="1957">1957</li>
							<li class="date-search-modal_select-option" data-date-val="1956">1956</li>
							<li class="date-search-modal_select-option" data-date-val="1955">1955</li>
							<li class="date-search-modal_select-option" data-date-val="1954">1954</li>
							<li class="date-search-modal_select-option" data-date-val="1953">1953</li>
							<li class="date-search-modal_select-option" data-date-val="1952">1952</li>
							<li class="date-search-modal_select-option" data-date-val="1951">1951</li>
							<li class="date-search-modal_select-option" data-date-val="1950">1950</li>
							<li class="date-search-modal_select-option" data-date-val="1949">1949</li>
							<li class="date-search-modal_select-option" data-date-val="1948">1948</li>
							<li class="date-search-modal_select-option" data-date-val="1947">1947</li>
							<li class="date-search-modal_select-option" data-date-val="1946">1946</li>
							<li class="date-search-modal_select-option" data-date-val="1945">1945</li>
							<li class="date-search-modal_select-option" data-date-val="1944">1944</li>
							<li class="date-search-modal_select-option" data-date-val="1943">1943</li>
							<li class="date-search-modal_select-option" data-date-val="1942">1942</li>
							<li class="date-search-modal_select-option" data-date-val="1941">1941</li>
							<li class="date-search-modal_select-option" data-date-val="1940">1940</li>
							<li class="date-search-modal_select-option" data-date-val="1939">1939</li>
							<li class="date-search-modal_select-option" data-date-val="1938">1938</li>
							<li class="date-search-modal_select-option" data-date-val="1937">1937</li>
							<li class="date-search-modal_select-option" data-date-val="1936">1936</li>
							<li class="date-search-modal_select-option" data-date-val="1935">1935</li>
							<li class="date-search-modal_select-option" data-date-val="1934">1934</li>
							<li class="date-search-modal_select-option" data-date-val="1933">1933</li>
							<li class="date-search-modal_select-option" data-date-val="1932">1932</li>
							<li class="date-search-modal_select-option" data-date-val="1931">1931</li>
							<li class="date-search-modal_select-option" data-date-val="1930">1930</li>
							<li class="date-search-modal_select-option" data-date-val="1929">1929</li>
							<li class="date-search-modal_select-option" data-date-val="1928">1928</li>
							<li class="date-search-modal_select-option" data-date-val="1927">1927</li>
							<li class="date-search-modal_select-option" data-date-val="1926">1926</li>
							<li class="date-search-modal_select-option" data-date-val="1925">1925</li>
							<li class="date-search-modal_select-option" data-date-val="1924">1924</li>
							<li class="date-search-modal_select-option" data-date-val="1923">1923</li>
							<li class="date-search-modal_select-option" data-date-val="1922">1922</li>
							<li class="date-search-modal_select-option" data-date-val="1921">1921</li>
							<li class="date-search-modal_select-option" data-date-val="1920">1920</li>
							<li class="date-search-modal_select-option" data-date-val="1919">1919</li>
							<li class="date-search-modal_select-option" data-date-val="1918">1918</li>
							<li class="date-search-modal_select-option" data-date-val="1917">1917</li>
							<li class="date-search-modal_select-option" data-date-val="1916">1916</li>
							<li class="date-search-modal_select-option" data-date-val="1915">1915</li>
							<li class="date-search-modal_select-option" data-date-val="1914">1914</li>
							<li class="date-search-modal_select-option" data-date-val="1913">1913</li>
							<li class="date-search-modal_select-option" data-date-val="1912">1912</li>
							<li class="date-search-modal_select-option" data-date-val="1911">1911</li>
							<li class="date-search-modal_select-option" data-date-val="1910">1910</li>
							<li class="date-search-modal_select-option" data-date-val="1909">1909</li>
							<li class="date-search-modal_select-option" data-date-val="1908">1908</li>
							<li class="date-search-modal_select-option" data-date-val="1907">1907</li>
							<li class="date-search-modal_select-option" data-date-val="1906">1906</li>
							<li class="date-search-modal_select-option" data-date-val="1905">1905</li>
							<li class="date-search-modal_select-option" data-date-val="1904">1904</li>
							<li class="date-search-modal_select-option" data-date-val="1903">1903</li>
							<li class="date-search-modal_select-option" data-date-val="1902">1902</li>
							<li class="date-search-modal_select-option" data-date-val="1901">1901</li>
							<li class="date-search-modal_select-option" data-date-val="1900">1900</li>
						</ul>
					</label>
					</span>
				</div>
				<p class="date-search-modal_error-message">Please try a different date</p>
				<button id="dateSearchSubmit" class="date-search-modal_submit-btn" data-day="31" data-month="8" data-year="2019" data-chart-slug="HSI">
					View Chart
				</button><img class="date-search-modal_loading-icon" src="" alt="Loading..."></div>
			<div class="date-search-modal_close-row">
				<button class="date-search-modal_close-btn date-search-modal-toggle">Cancel</button>
			</div>
		</div>
	</div>
	<!-- Date Picker Ends -->
	
	<div class="ad-container leaderboard-top"><div class="leaderboard-wrapper"></div></div>
	
	<!-- Video Listing -->
	<div class="container chart-container container-xxlight-grey container-no-side-padding">
		<div class="chart-details ">
			<div class="chart-list chart-details_left-rail" >
				<img class="chart-video_thumbnail-overlay-play-btn-img" src="<?php echo get_template_directory_uri(); ?>/images/thumb-play-btn.svg" alt="Play">
				<?php foreach ($posts as $key => $song): ?>
					<div class="chart-list-item" data-rank="1" data-artist="Shawn Mendes &amp; Camila Cabello" data-title="Senorita" data-has-content="true">
						<div class="chart-list-item_first-row chart-list-item_cursor-pointer">
							<div class="chart-list-item_position ">
								<div class="chart-list-item_rank ">
									<?php echo $key+1; ?>
								</div>
								<div class="chart-list-item_award">
									<i class="fa fa-star"></i>
								</div>
							</div>
							<div class="chart-list-item_image-wrapper">
								<div class="chart-list-item_video-play-overlay">
									<img class="play-icon" src="<?php echo get_template_directory_uri(); ?>/images/thumb-play-btn.svg" alt="Play">
									<span class="now-playing-icon"></span>
								</div>
								<div class="chart-list-item_trend-icon">
								<?php if (get_field("last_week_1",$song->ID) > get_field("last_week_2",$song->ID)): ?>
									<img src="<?php echo get_template_directory_uri(); ?>/images/arrow-up.svg">
								<?php else: ?>
									<img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down.svg">
								<?php endif ?>
								</div>
								<img src="<?php echo get_the_post_thumbnail_url($song->ID); ?>" class="chart-list-item_image" alt="Shawn Mendes &amp; Camila Cabello Senorita Billboard Hot 100" >
							</div>
							<div class="chart-list-item_text-wrapper">
								<div class="chart-list-item_text chart-list-item_text-has-video">
									<div class="chart-list-item_title">
										<span class="chart-list-item_title-text"><?php echo $song->post_title; ?></span>
									</div>
									<div class="chart-list-item_artist">
										<?php 
											$term_list = wp_get_post_terms( $song->ID, 'singer', array( 'fields' => 'names' ) );
											print_r( implode(" & ", $term_list)); 
										?>
									</div>
									<div class="chart-list-item_lyrics ">
										<a href="#">
											<span class="hidden-mobile show-expanded-mobile-inline">
												<?php 
													$term_list = wp_get_post_terms( $song->ID, 'lyricist', array( 'fields' => 'names' ) );
													print_r( implode(" & ", $term_list)); 
												?>
											</span>
										</a>
									</div>
								</div>
							</div>
							<div class="chart-list-item_video-icon-wrapper">
								<div id="<?php the_field("youtube_id",$song->ID); ?>" data-val="<?php echo $key+1; ?>" title="<?php echo $song->post_title; ?>" class="chart-list-item_video-icon chart-list-item_video-play-trigger play_on_click"></div>
								<span class="now-playing-icon"></span>
							</div>
							<div class="chart-list-item_chevron-wrapper">
								<i class="fa fa-chevron-down"></i>
							</div>
						</div>
						<div class="chart-list-item_extra-info">
							<div class="chart-list-item_extra-info-shadow"></div>
							<div class="chart-video_wrapper">
								<div class="chart-list-item_expanded-header">
									<div class="chart-video_spinner"></div>
								</div>
								<div class="chart-videos">
									<div class="chart-video chart-video_bb-news chart-video-active">
										<div class="chart-video_thumbnail">
											<img class="chart-video_thumbnail-poster" src="<?php echo get_the_post_thumbnail_url($song->ID); ?>" alt="Image">
											<div class="chart-video_information-gradient"></div>
											<div class="chart-video_information ellipsis">
												<p><?php echo $song->post_content; ?></p>
											</div>
											<div class="chart-video_thumbnail-overlay">
												<div class="chart-video_thumbnail-overlay-play-btn"><img id="<?php the_field("youtube_id",$song->ID); ?>" data-val="<?php echo $key+1; ?>" title="<?php echo $song->post_title; ?>" class="chart-video_thumbnail-overlay-play-btn-img play_on_click" src="<?php echo get_template_directory_uri(); ?>/images/thumb-play-btn.svg" alt="Play"></div>
												<span class="now-playing-icon"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="chart-list-item_stats">
								<hr class="chart-list-item_stats-divider chart-list-item_stats-divider-top">
								<div class="chart-list-item_stats-cell  basic-user  chart-list-item_stats-cell-first-cell">
									<div class="chart-list-item_stats-icon fa fa-arrow-up fa-rotate-45"></div>
									<div class="chart-list-item_last-week">3</div>
									PREVIOUS WEEK
								</div>
								<div class="chart-list-item_stats-cell  basic-user ">
									<div class="chart-list-item_stats-icon fa fa-arrow-up fa-rotate-45"></div>
									<div class="chart-list-item_last-week"><?php the_field("last_week_1",$song->ID); ?></div>
									LAST WEEK 1
								</div>
								<div class="chart-list-item_stats-cell  basic-user ">
									<div class="chart-list-item_stats-icon fa fa-line-chart"></div>
									<div class="chart-list-item_weeks-at-one"><?php the_field("last_week_2",$song->ID); ?></div>
									LAST WEEK 2
								</div>
								<div class="chart-list-item_stats-cell  basic-user   chart-list-item_stats-cell-no-border-right">
									<div class="chart-list-item_stats-icon fa fa-clock-o"></div>
									<div class="chart-list-item_weeks-on-chart"><?php the_field("last_week_3",$song->ID); ?></div>
									LAST WEEK 3
								</div>
								<hr class="chart-list-item_stats-divider chart-list-item_stats-divider-bottom">
							</div>
							<div class="chart-list-item_awards">
								<div class="chart-list-item_award-icon chart-list-item_award-icon-label"><i class="fa fa-star"></i> Awards</div>
								<div class="chart-list-item_award-icon "><i class="fa fa-microphone"></i> Biggest gain in airplay</div>
								<div class="chart-list-item_award-icon chart-list-item_award-icon-last"><i class="fa fa-dot-circle-o"></i> Gains in performance</div>
							</div>
						</div>
					</div>
				<?php endforeach ?>
				</div>
			</div>
			<div class="bb_sidebar">
				<?php get_sidebar( 'primary' ); ?>
			</div>
		</div>
	</div>
	<!-- Video Listing Ends -->
</main>
<!-- Main Ends -->	
<?php get_footer(); ?>