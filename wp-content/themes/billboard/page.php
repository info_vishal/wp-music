<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage BillBoard
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php

	$the_query = new WP_Query( array('post_type' => 'song') );
	$posts = $the_query->posts;
	wp_reset_postdata();
	foreach ($posts as $key => $song) {
		$JSON = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=statistics&id=".get_field("youtube_id",$song->ID)."&key=AIzaSyAp-9iqoKZPf_1oNAZJablduKY4CZZZdcA");
		$JSON_Data = json_decode($JSON);

		$new_total_count = $JSON_Data->items[0]->statistics->viewCount;
		$old_total_count = get_field("total_count",$song->ID);
		$last_week_1 = get_field("last_week_1",$song->ID);
		$last_week_2 = get_field("last_week_2",$song->ID);
		$last_week_3 = get_field("last_week_3",$song->ID);
		$last_week_4 = get_field("last_week_4",$song->ID);
		$new_inc_views = $new_total_count-$old_total_count;


		update_post_meta($song->ID,"last_week_1",$new_inc_views);
		update_post_meta($song->ID,"last_week_2",$last_week_1);
		update_post_meta($song->ID,"last_week_3",$last_week_2);
		update_post_meta($song->ID,"last_week_4",$last_week_3);
		update_post_meta($song->ID,"total_count",$new_total_count);
	}

get_footer();
